import {
  assert,
  describe,
  test,
  clearStore,
  beforeAll,
  afterAll
} from "matchstick-as/assembly/index"
import { Address, BigInt } from "@graphprotocol/graph-ts"
import { BuyTables } from "../generated/schema"
import { BuyTables as BuyTablesEvent } from "../generated/RandomPool/RandomPool"
import { handleBuyTables } from "../src/random-pool"
import { createBuyTablesEvent } from "./random-pool-utils"

// Tests structure (matchstick-as >=0.5.0)
// https://thegraph.com/docs/en/developer/matchstick/#tests-structure-0-5-0

describe("Describe entity assertions", () => {
  beforeAll(() => {
    let user = Address.fromString("0x0000000000000000000000000000000000000001")
    let referral = Address.fromString(
      "0x0000000000000000000000000000000000000001"
    )
    let level = BigInt.fromI32(234)
    let amount = BigInt.fromI32(234)
    let newBuyTablesEvent = createBuyTablesEvent(user, referral, level, amount)
    handleBuyTables(newBuyTablesEvent)
  })

  afterAll(() => {
    clearStore()
  })

  // For more test scenarios, see:
  // https://thegraph.com/docs/en/developer/matchstick/#write-a-unit-test

  test("BuyTables created and stored", () => {
    assert.entityCount("BuyTables", 1)

    // 0xa16081f360e3847006db660bae1c6d1b2e17ec2a is the default address used in newMockEvent() function
    assert.fieldEquals(
      "BuyTables",
      "0xa16081f360e3847006db660bae1c6d1b2e17ec2a-1",
      "user",
      "0x0000000000000000000000000000000000000001"
    )
    assert.fieldEquals(
      "BuyTables",
      "0xa16081f360e3847006db660bae1c6d1b2e17ec2a-1",
      "referral",
      "0x0000000000000000000000000000000000000001"
    )
    assert.fieldEquals(
      "BuyTables",
      "0xa16081f360e3847006db660bae1c6d1b2e17ec2a-1",
      "level",
      "234"
    )
    assert.fieldEquals(
      "BuyTables",
      "0xa16081f360e3847006db660bae1c6d1b2e17ec2a-1",
      "amount",
      "234"
    )

    // More assert options:
    // https://thegraph.com/docs/en/developer/matchstick/#asserts
  })
})
