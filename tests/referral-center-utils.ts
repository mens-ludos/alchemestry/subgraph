import { newMockEvent } from "matchstick-as"
import { ethereum, Address, BigInt } from "@graphprotocol/graph-ts"
import {
  AddRewards,
  ClaimRewards,
  CreateUser,
  Initialized
} from "../generated/ReferralCenter/ReferralCenter"

export function createAddRewardsEvent(
  user: Address,
  amount: BigInt
): AddRewards {
  let addRewardsEvent = changetype<AddRewards>(newMockEvent())

  addRewardsEvent.parameters = new Array()

  addRewardsEvent.parameters.push(
    new ethereum.EventParam("user", ethereum.Value.fromAddress(user))
  )
  addRewardsEvent.parameters.push(
    new ethereum.EventParam("amount", ethereum.Value.fromUnsignedBigInt(amount))
  )

  return addRewardsEvent
}

export function createClaimRewardsEvent(
  user: Address,
  amount: BigInt
): ClaimRewards {
  let claimRewardsEvent = changetype<ClaimRewards>(newMockEvent())

  claimRewardsEvent.parameters = new Array()

  claimRewardsEvent.parameters.push(
    new ethereum.EventParam("user", ethereum.Value.fromAddress(user))
  )
  claimRewardsEvent.parameters.push(
    new ethereum.EventParam("amount", ethereum.Value.fromUnsignedBigInt(amount))
  )

  return claimRewardsEvent
}

export function createCreateUserEvent(
  user: Address,
  referrer: Address
): CreateUser {
  let createUserEvent = changetype<CreateUser>(newMockEvent())

  createUserEvent.parameters = new Array()

  createUserEvent.parameters.push(
    new ethereum.EventParam("user", ethereum.Value.fromAddress(user))
  )
  createUserEvent.parameters.push(
    new ethereum.EventParam("referrer", ethereum.Value.fromAddress(referrer))
  )

  return createUserEvent
}

export function createInitializedEvent(version: BigInt): Initialized {
  let initializedEvent = changetype<Initialized>(newMockEvent())

  initializedEvent.parameters = new Array()

  initializedEvent.parameters.push(
    new ethereum.EventParam(
      "version",
      ethereum.Value.fromUnsignedBigInt(version)
    )
  )

  return initializedEvent
}
