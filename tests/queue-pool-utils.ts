import { newMockEvent } from "matchstick-as"
import { ethereum, Address, BigInt } from "@graphprotocol/graph-ts"
import {
  AddTables,
  ClaimRewards,
  FulfillTable,
  Initialized,
  OwnershipTransferred
} from "../generated/QueuePool/QueuePool"

export function createAddTablesEvent(
  caller: Address,
  tablesCount: BigInt,
  tablePrice: BigInt
): AddTables {
  let addTablesEvent = changetype<AddTables>(newMockEvent())

  addTablesEvent.parameters = new Array()

  addTablesEvent.parameters.push(
    new ethereum.EventParam("caller", ethereum.Value.fromAddress(caller))
  )
  addTablesEvent.parameters.push(
    new ethereum.EventParam(
      "tablesCount",
      ethereum.Value.fromUnsignedBigInt(tablesCount)
    )
  )
  addTablesEvent.parameters.push(
    new ethereum.EventParam(
      "tablePrice",
      ethereum.Value.fromUnsignedBigInt(tablePrice)
    )
  )

  return addTablesEvent
}

export function createClaimRewardsEvent(
  caller: Address,
  claimedAmount: BigInt
): ClaimRewards {
  let claimRewardsEvent = changetype<ClaimRewards>(newMockEvent())

  claimRewardsEvent.parameters = new Array()

  claimRewardsEvent.parameters.push(
    new ethereum.EventParam("caller", ethereum.Value.fromAddress(caller))
  )
  claimRewardsEvent.parameters.push(
    new ethereum.EventParam(
      "claimedAmount",
      ethereum.Value.fromUnsignedBigInt(claimedAmount)
    )
  )

  return claimRewardsEvent
}

export function createFulfillTableEvent(
  caller: Address,
  tablesOwner: Address,
  tableFulfillAmount: BigInt,
  fulfilled: BigInt
): FulfillTable {
  let fulfillTableEvent = changetype<FulfillTable>(newMockEvent())

  fulfillTableEvent.parameters = new Array()

  fulfillTableEvent.parameters.push(
    new ethereum.EventParam("caller", ethereum.Value.fromAddress(caller))
  )
  fulfillTableEvent.parameters.push(
    new ethereum.EventParam(
      "tablesOwner",
      ethereum.Value.fromAddress(tablesOwner)
    )
  )
  fulfillTableEvent.parameters.push(
    new ethereum.EventParam(
      "tableFulfillAmount",
      ethereum.Value.fromUnsignedBigInt(tableFulfillAmount)
    )
  )
  fulfillTableEvent.parameters.push(
    new ethereum.EventParam(
      "fulfilled",
      ethereum.Value.fromUnsignedBigInt(fulfilled)
    )
  )

  return fulfillTableEvent
}

export function createInitializedEvent(version: BigInt): Initialized {
  let initializedEvent = changetype<Initialized>(newMockEvent())

  initializedEvent.parameters = new Array()

  initializedEvent.parameters.push(
    new ethereum.EventParam(
      "version",
      ethereum.Value.fromUnsignedBigInt(version)
    )
  )

  return initializedEvent
}

export function createOwnershipTransferredEvent(
  previousOwner: Address,
  newOwner: Address
): OwnershipTransferred {
  let ownershipTransferredEvent = changetype<OwnershipTransferred>(
    newMockEvent()
  )

  ownershipTransferredEvent.parameters = new Array()

  ownershipTransferredEvent.parameters.push(
    new ethereum.EventParam(
      "previousOwner",
      ethereum.Value.fromAddress(previousOwner)
    )
  )
  ownershipTransferredEvent.parameters.push(
    new ethereum.EventParam("newOwner", ethereum.Value.fromAddress(newOwner))
  )

  return ownershipTransferredEvent
}
