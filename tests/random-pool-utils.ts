import { newMockEvent } from "matchstick-as"
import { ethereum, Address, BigInt } from "@graphprotocol/graph-ts"
import {
  BuyTables,
  ClaimRewards,
  FulfillPool,
  FulfillTable,
  Initialized,
  NewReferral,
  OwnershipTransferred,
  PerformUpkeep
} from "../generated/RandomPool/RandomPool"

export function createBuyTablesEvent(
  user: Address,
  referral: Address,
  level: BigInt,
  amount: BigInt
): BuyTables {
  let buyTablesEvent = changetype<BuyTables>(newMockEvent())

  buyTablesEvent.parameters = new Array()

  buyTablesEvent.parameters.push(
    new ethereum.EventParam("user", ethereum.Value.fromAddress(user))
  )
  buyTablesEvent.parameters.push(
    new ethereum.EventParam("referral", ethereum.Value.fromAddress(referral))
  )
  buyTablesEvent.parameters.push(
    new ethereum.EventParam("level", ethereum.Value.fromUnsignedBigInt(level))
  )
  buyTablesEvent.parameters.push(
    new ethereum.EventParam("amount", ethereum.Value.fromUnsignedBigInt(amount))
  )

  return buyTablesEvent
}

export function createClaimRewardsEvent(
  user: Address,
  level: BigInt,
  claimedAmount: BigInt
): ClaimRewards {
  let claimRewardsEvent = changetype<ClaimRewards>(newMockEvent())

  claimRewardsEvent.parameters = new Array()

  claimRewardsEvent.parameters.push(
    new ethereum.EventParam("user", ethereum.Value.fromAddress(user))
  )
  claimRewardsEvent.parameters.push(
    new ethereum.EventParam("level", ethereum.Value.fromUnsignedBigInt(level))
  )
  claimRewardsEvent.parameters.push(
    new ethereum.EventParam(
      "claimedAmount",
      ethereum.Value.fromUnsignedBigInt(claimedAmount)
    )
  )

  return claimRewardsEvent
}

export function createFulfillPoolEvent(
  level: BigInt,
  isFulfilled: boolean,
  leftToFulfill: BigInt
): FulfillPool {
  let fulfillPoolEvent = changetype<FulfillPool>(newMockEvent())

  fulfillPoolEvent.parameters = new Array()

  fulfillPoolEvent.parameters.push(
    new ethereum.EventParam("level", ethereum.Value.fromUnsignedBigInt(level))
  )
  fulfillPoolEvent.parameters.push(
    new ethereum.EventParam(
      "isFulfilled",
      ethereum.Value.fromBoolean(isFulfilled)
    )
  )
  fulfillPoolEvent.parameters.push(
    new ethereum.EventParam(
      "leftToFulfill",
      ethereum.Value.fromUnsignedBigInt(leftToFulfill)
    )
  )

  return fulfillPoolEvent
}

export function createFulfillTableEvent(
  user: Address,
  level: BigInt
): FulfillTable {
  let fulfillTableEvent = changetype<FulfillTable>(newMockEvent())

  fulfillTableEvent.parameters = new Array()

  fulfillTableEvent.parameters.push(
    new ethereum.EventParam("user", ethereum.Value.fromAddress(user))
  )
  fulfillTableEvent.parameters.push(
    new ethereum.EventParam("level", ethereum.Value.fromUnsignedBigInt(level))
  )

  return fulfillTableEvent
}

export function createInitializedEvent(version: BigInt): Initialized {
  let initializedEvent = changetype<Initialized>(newMockEvent())

  initializedEvent.parameters = new Array()

  initializedEvent.parameters.push(
    new ethereum.EventParam(
      "version",
      ethereum.Value.fromUnsignedBigInt(version)
    )
  )

  return initializedEvent
}

export function createNewReferralEvent(
  user: Address,
  referrer: Address,
  referrerFee: BigInt
): NewReferral {
  let newReferralEvent = changetype<NewReferral>(newMockEvent())

  newReferralEvent.parameters = new Array()

  newReferralEvent.parameters.push(
    new ethereum.EventParam("user", ethereum.Value.fromAddress(user))
  )
  newReferralEvent.parameters.push(
    new ethereum.EventParam("referrer", ethereum.Value.fromAddress(referrer))
  )
  newReferralEvent.parameters.push(
    new ethereum.EventParam(
      "referrerFee",
      ethereum.Value.fromUnsignedBigInt(referrerFee)
    )
  )

  return newReferralEvent
}

export function createOwnershipTransferredEvent(
  previousOwner: Address,
  newOwner: Address
): OwnershipTransferred {
  let ownershipTransferredEvent = changetype<OwnershipTransferred>(
    newMockEvent()
  )

  ownershipTransferredEvent.parameters = new Array()

  ownershipTransferredEvent.parameters.push(
    new ethereum.EventParam(
      "previousOwner",
      ethereum.Value.fromAddress(previousOwner)
    )
  )
  ownershipTransferredEvent.parameters.push(
    new ethereum.EventParam("newOwner", ethereum.Value.fromAddress(newOwner))
  )

  return ownershipTransferredEvent
}

export function createPerformUpkeepEvent(
  caller: Address,
  levelToFullfill: BigInt,
  wordsRequested: BigInt
): PerformUpkeep {
  let performUpkeepEvent = changetype<PerformUpkeep>(newMockEvent())

  performUpkeepEvent.parameters = new Array()

  performUpkeepEvent.parameters.push(
    new ethereum.EventParam("caller", ethereum.Value.fromAddress(caller))
  )
  performUpkeepEvent.parameters.push(
    new ethereum.EventParam(
      "levelToFullfill",
      ethereum.Value.fromUnsignedBigInt(levelToFullfill)
    )
  )
  performUpkeepEvent.parameters.push(
    new ethereum.EventParam(
      "wordsRequested",
      ethereum.Value.fromUnsignedBigInt(wordsRequested)
    )
  )

  return performUpkeepEvent
}
