import {
  assert,
  describe,
  test,
  clearStore,
  beforeAll,
  afterAll
} from "matchstick-as/assembly/index"
import { Address, BigInt } from "@graphprotocol/graph-ts"
import { AddTables } from "../generated/schema"
import { AddTables as AddTablesEvent } from "../generated/QueuePool/QueuePool"
import { handleAddTables } from "../src/queue-pool"
import { createAddTablesEvent } from "./queue-pool-utils"

// Tests structure (matchstick-as >=0.5.0)
// https://thegraph.com/docs/en/developer/matchstick/#tests-structure-0-5-0

describe("Describe entity assertions", () => {
  beforeAll(() => {
    let caller = Address.fromString(
      "0x0000000000000000000000000000000000000001"
    )
    let tablesCount = BigInt.fromI32(234)
    let tablePrice = BigInt.fromI32(234)
    let newAddTablesEvent = createAddTablesEvent(
      caller,
      tablesCount,
      tablePrice
    )
    handleAddTables(newAddTablesEvent)
  })

  afterAll(() => {
    clearStore()
  })

  // For more test scenarios, see:
  // https://thegraph.com/docs/en/developer/matchstick/#write-a-unit-test

  test("AddTables created and stored", () => {
    assert.entityCount("AddTables", 1)

    // 0xa16081f360e3847006db660bae1c6d1b2e17ec2a is the default address used in newMockEvent() function
    assert.fieldEquals(
      "AddTables",
      "0xa16081f360e3847006db660bae1c6d1b2e17ec2a-1",
      "caller",
      "0x0000000000000000000000000000000000000001"
    )
    assert.fieldEquals(
      "AddTables",
      "0xa16081f360e3847006db660bae1c6d1b2e17ec2a-1",
      "tablesCount",
      "234"
    )
    assert.fieldEquals(
      "AddTables",
      "0xa16081f360e3847006db660bae1c6d1b2e17ec2a-1",
      "tablePrice",
      "234"
    )

    // More assert options:
    // https://thegraph.com/docs/en/developer/matchstick/#asserts
  })
})
