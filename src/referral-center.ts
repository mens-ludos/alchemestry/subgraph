import {
  AddRewards as AddRewardsEvent,
  ClaimRewards as ClaimRewardsEvent,
  CreateUser as CreateUserEvent,
  Initialized as InitializedEvent,
} from "../generated/ReferralCenter/ReferralCenter"
import {
  AddRewards,
  ReferralCenterClaimRewards,
  CreateUser,
  ReferralCenterInitialized,
} from "../generated/schema"

export function handleAddRewards(event: AddRewardsEvent): void {
  let entity = new AddRewards(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.user = event.params.user
  entity.amount = event.params.amount

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleClaimRewards(event: ClaimRewardsEvent): void {
  let entity = new ReferralCenterClaimRewards(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.user = event.params.user
  entity.amount = event.params.amount

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleCreateUser(event: CreateUserEvent): void {
  let entity = new CreateUser(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.user = event.params.user
  entity.referrer = event.params.referrer

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleInitialized(event: InitializedEvent): void {
  let entity = new ReferralCenterInitialized(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.version = event.params.version

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}
