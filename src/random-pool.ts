import {
  BuyTables as BuyTablesEvent,
  ClaimRewards as ClaimRewardsEvent,
  FulfillPool as FulfillPoolEvent,
  FulfillTable as FulfillTableEvent,
  Initialized as InitializedEvent,
  NewReferral as NewReferralEvent,
  OwnershipTransferred as OwnershipTransferredEvent,
  PerformUpkeep as PerformUpkeepEvent,
} from "../generated/RandomPool/RandomPool"
import {
  BuyTables,
  RandomPoolClaimRewards,
  FulfillPool,
  RandomPoolFulfillTable,
  RandomPoolInitialized,
  NewReferral,
  RandomPoolOwnershipTransferred,
  PerformUpkeep,
} from "../generated/schema"

export function handleBuyTables(event: BuyTablesEvent): void {
  let entity = new BuyTables(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.user = event.params.user
  entity.referral = event.params.referral
  entity.level = event.params.level
  entity.amount = event.params.amount

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleClaimRewards(event: ClaimRewardsEvent): void {
  let entity = new RandomPoolClaimRewards(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.user = event.params.user
  entity.level = event.params.level
  entity.claimedAmount = event.params.claimedAmount

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleFulfillPool(event: FulfillPoolEvent): void {
  let entity = new FulfillPool(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.level = event.params.level
  entity.isFulfilled = event.params.isFulfilled
  entity.leftToFulfill = event.params.leftToFulfill

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleFulfillTable(event: FulfillTableEvent): void {
  let entity = new RandomPoolFulfillTable(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.user = event.params.user
  entity.level = event.params.level

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleInitialized(event: InitializedEvent): void {
  let entity = new RandomPoolInitialized(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.version = event.params.version

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleNewReferral(event: NewReferralEvent): void {
  let entity = new NewReferral(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.user = event.params.user
  entity.referrer = event.params.referrer
  entity.referrerFee = event.params.referrerFee

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handleOwnershipTransferred(
  event: OwnershipTransferredEvent,
): void {
  let entity = new RandomPoolOwnershipTransferred(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.previousOwner = event.params.previousOwner
  entity.newOwner = event.params.newOwner

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}

export function handlePerformUpkeep(event: PerformUpkeepEvent): void {
  let entity = new PerformUpkeep(
    event.transaction.hash.concatI32(event.logIndex.toI32()),
  )
  entity.caller = event.params.caller
  entity.levelToFullfill = event.params.levelToFullfill
  entity.wordsRequested = event.params.wordsRequested

  entity.blockNumber = event.block.number
  entity.blockTimestamp = event.block.timestamp
  entity.transactionHash = event.transaction.hash

  entity.save()
}
